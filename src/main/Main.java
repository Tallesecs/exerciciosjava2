package main;

import exercicios.Exercicio1;
import exercicios.Exercicio10;
import exercicios.Exercicio2;
import exercicios.Exercicio3;
import exercicios.Exercicio4;
import exercicios.Exercicio5;
import exercicios.Exercicio7;
import exercicios.Exercicio8;
import exercicios.Exercicio9;
import exercicios.Exercicios6;

public class Main {

	public static void main(String[] args) {

		Exercicio1 resultado1 = new Exercicio1();
		Exercicio2 resultado2 = new Exercicio2();
		Exercicio3 resultado3 = new Exercicio3();
		Exercicio4 resultado4 = new Exercicio4();
		Exercicio5 resultado5 = new Exercicio5();
		Exercicios6 resultado6 = new Exercicios6();
		Exercicio7 resultado7 = new Exercicio7();
		Exercicio8 resultado8 = new Exercicio8();
		Exercicio9 resultado9 = new Exercicio9();
		Exercicio10 resultado10 = new Exercicio10();
		
		resultado1.soma();
		resultado2.operaes();
		resultado3.consumo();
		resultado4.vendas();
		resultado5.notas();
		resultado6.valoresAB();
		resultado7.temperatura();
		resultado8.conv();
		resultado9.rendimento();
		resultado10.prestacao();
	}

}
